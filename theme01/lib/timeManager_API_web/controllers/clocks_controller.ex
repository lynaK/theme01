defmodule TodolistWeb.ClocksController do
  use TodolistWeb, :controller

  alias Todolist.Repo
  alias Todolist.Clock
  alias Todolist.Clock.Clocks

  action_fallback TodolistWeb.FallbackController

  def index(conn, _params) do
    clocks = Clock.list_clocks()
    render(conn, "index.json", clocks: clocks)
  end

  def create(conn, %{"clocks" => clocks_params}) do
    with {:ok, %Clocks{} = clocks} <- Clock.create_clocks(clocks_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.clocks_path(conn, :show, clocks))
      |> render("show.json", clocks: clocks)
    end
  end

  def create(conn, params) do
    clocks = %{
      time: NaiveDateTime.utc_now,
      status: params["status"],
      users: params["users"],
    }

    with {:ok, %Clocks{} = clocks} <- Clock.create_clocks(clocks) do
      json(conn,%{res: "OK"})
    end
  end

  def showByUser(conn, %{"userID" => param_user}) do
    clocks = Clocks
    |> QueryBuilder.where(users: param_user)
    |> Repo.all()
    render(conn, "index.json", clocks: clocks)
  end

  def show(conn, %{"id" => id}) do
    clocks = Clock.get_clocks!(id)
    render(conn, "show.json", clocks: clocks)
  end

  def update(conn, %{"id" => id, "clocks" => clocks_params}) do
    clocks = Clock.get_clocks!(id)

    with {:ok, %Clocks{} = clocks} <- Clock.update_clocks(clocks, clocks_params) do
      render(conn, "show.json", clocks: clocks)
    end
  end

  def delete(conn, %{"id" => id}) do
    clocks = Clock.get_clocks!(id)

    with {:ok, %Clocks{}} <- Clock.delete_clocks(clocks) do
      send_resp(conn, :no_content, "")
    end
  end
end
