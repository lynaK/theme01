defmodule TodolistWeb.UsersController do
  use TodolistWeb, :controller

  alias Todolist.User
  alias Todolist.User.Users

  action_fallback TodolistWeb.FallbackController



  def index(conn, %{"username" => userName,"email" => email}) do
    user = User.getUserByUserNameAndEmail!(userName, email)
    render(conn, "show.json", user: user)
  end

  def index(conn, _params) do
    users = User.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"users" => users_params}) do
    with {:ok, %Users{} = users} <- User.create_users(users_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.users_path(conn, :show, users))
      |> render("show.json", users: users)
    end
  end

  def show(conn, %{"id" => id}) do
    users = User.get_users!(id)
    render(conn, "show.json", users: users)
  end

  def update(conn, %{"id" => id, "users" => users_params}) do
    users = User.get_users!(id)

    with {:ok, %Users{} = users} <- User.update_users(users, users_params) do
      render(conn, "show.json", users: users)
    end
  end

  def delete(conn, %{"id" => id}) do
    users = User.get_users!(id)

    with {:ok, %Users{}} <- User.delete_users(users) do
      send_resp(conn, :no_content, "")
    end
  end

  def userRole(conn, _params) do
    users = User.getUserRole()
    render(conn, "index.json", users: users)
  end

  def getUserManager(conn, _params) do
    users = User.getUserManager()
    render(conn, "index.json", users: users)
  end

  def signin(conn, %{"email" => email,"password" => password}) do
    users = User.getUserByUserEmailPwd!(email, password)
    render(conn, "show.json", users: users)
  end
end
