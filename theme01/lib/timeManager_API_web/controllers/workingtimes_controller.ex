defmodule TodolistWeb.WorkingtimesController do
  import Ecto.Query
  require Logger
  use TodolistWeb, :controller

  alias Todolist.Repo
  alias Todolist.WorkingTimes
  alias Todolist.WorkingTimes.Workingtimes

  action_fallback TodolistWeb.FallbackController

  def index(conn, _params) do
    working_times = WorkingTimes.list_working_times()
    render(conn, "index.json", working_times: working_times)
  end

  def create(conn, params) do
    workingtime = %{
      start: params["start"],
      end: params["end"],
      users: params["users"],
    }

    with {:ok, %Workingtimes{} = workingtime} <- WorkingTimes.create_workingtimes(workingtime) do
      json(conn,%{res: "OK"})
    end
  end

  def showOne(conn, %{"userID" => param_user, "workingtimesID" => param_workingid}) do
    workingtimes = Workingtimes
    |> QueryBuilder.where(users: param_user)
    |> QueryBuilder.where(id: param_workingid)
    |> Repo.one()
    render(conn, "show.json", workingtimes: workingtimes)
  end

  def getWorkingTimes(conn, %{"userId" => userId, "start"=> starttime, "end"=> endtime}) do
    working_times = WorkingTimes.get_workingtimesByUserBetweenTimes!(userId, starttime, endtime)
    render(conn, "index.json", working_times: working_times)
  end

  def showAll(conn, %{"userId" => userId}) do
    working_times = WorkingTimes.get_workingtimesByUser!(userId)
    render(conn, "index.json", working_times: working_times)
  end


  @spec show(Plug.Conn.t(), map) :: Plug.Conn.t()
  def show(conn, %{"id" => id}) do
    workingtimes = WorkingTimes.get_workingtimes!(id)
    render(conn, "show.json", workingtimes: workingtimes)
  end


  def update(conn, %{"id" => id, "workingtimes" => workingtimes_params}) do
    workingtimes = WorkingTimes.get_workingtimes!(id)

    with {:ok, %Workingtimes{} = workingtimes} <- WorkingTimes.update_workingtimes(workingtimes, workingtimes_params) do
      render(conn, "show.json", workingtimes: workingtimes)
    end
  end

  def delete(conn, %{"id" => id}) do
    workingtimes = WorkingTimes.get_workingtimes!(id)

    with {:ok, %Workingtimes{}} <- WorkingTimes.delete_workingtimes(workingtimes) do
      send_resp(conn, :no_content, "")
    end
  end
end
