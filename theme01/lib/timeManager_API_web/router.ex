defmodule TodolistWeb.Router do
  use TodolistWeb, :router

  pipeline :api do
    plug CORSPlug, origin: ["*", "http://localhost:8080"], methods: ["GET", "POST", "OPTIONS", "DELETE", "PUT"], max_age: 86400
    plug :accepts, ["json"]
  end


  scope "/api", TodolistWeb do
    pipe_through :api
    options "/clocks/:userId", ClocksController, :options
    options "/workingtimes/:id", WorkingtimesController, :options
    options "/workingtimes/:userId", WorkingtimesController, :options
    options "/users", UsersController, :options
    options "/users/:userId", UsersController, :options
    get "/users/managers", UsersController, :getUserManager
    get "/users/signin", UsersController, :signin
    get "/users/role", UsersController, :userRole
    resources "/users", UsersController, except: [:new, :edit]
    get "/workingtimes", WorkingtimesController, :index
    get "/workingtimes/:userId", WorkingtimesController ,:showAll
    delete "/workingtimes/:id", WorkingtimesController, :delete
    put "/workingtimes/:id", WorkingtimesController, :update
    #resources "/workingtimes", WorkingtimesController, except: [:new, :edit]
    #resources "/clocks", ClocksController, except: [:new, :edit]
    resources "/team", TeamController, except: [:new, :edit]
    resources "/role", RoleController, except: [:new, :edit]
    get "/workingtimes/:userID/:workingtimesID", WorkingtimesController ,:showOne
    post "/clocks/:users", ClocksController, :create
    post "/workingtimes/:users", WorkingtimesController, :create
    get "/clocks/:userID", ClocksController, :showByUser
    get "/clocks", ClocksController, :index
    put "/clocks/:id", ClocksController, :update
    delete "/clocks/:id", ClocksController, :delete
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: TodolistWeb.Telemetry
    end
  end
end
