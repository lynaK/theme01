defmodule Todolist.Repo do
  use Ecto.Repo,
    otp_app: :timeManager_API,
    adapter: Ecto.Adapters.Postgres
end
