defmodule Todolist.Clock.Clocks do
  use Ecto.Schema
  use QueryBuilder
  import Ecto.Changeset

  schema "clocks" do
    field :status, :boolean, default: false
    field :time, :naive_datetime
    field :users, :id

    timestamps()
  end

  @doc false
  def changeset(clocks, attrs) do
    clocks
    |> cast(attrs, [:time, :status,:users])
    |> validate_required([:users, :time, :status])
  end
end
