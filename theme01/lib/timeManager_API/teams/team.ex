defmodule Todolist.Teams.Team do
  use Ecto.Schema
  import Ecto.Changeset

  schema "team" do
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [])
    |> validate_required([])
  end
end
