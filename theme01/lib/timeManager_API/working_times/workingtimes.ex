defmodule Todolist.WorkingTimes.Workingtimes do
  use Ecto.Schema

  use QueryBuilder

  import Ecto.Changeset

  schema "working_times" do
    field :end, :naive_datetime
    field :start, :naive_datetime
    field :users, :id

    timestamps()
  end

  @doc false
  def changeset(workingtimes, attrs) do
    workingtimes
    |> cast(attrs, [:start, :end, :users])
    |> validate_required([:start, :end, :users])
  end
end
