mix ecto.create  // Créer la BDD
mix phx.gen.json User Users users username:string email:string //Générer la table + controller + vue
Ajouter la route dans le fichier router.ex dans la partie API
mix phx.routes // S'assurer que les routes sont OK
mix ecto.migrate // Migration BDD
mix phx.server //Lancer l'api