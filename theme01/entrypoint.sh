#!/bin/bash
mix deps.get
mix ecto.drop
mix ecto.create
mix ecto.migrate
mix run priv/repo/seeds/role_seed.exs
mix run priv/repo/seeds/user_seeder.exs
mix run priv/repo/seeds/clocks_seeder.exs
mix run priv/repo/seeds/workingtimes_seeder.exs
mix phx.server