alias Todolist.Repo
alias Todolist.Roles.Role

Repo.insert! %Role{
  name: "admin"
}

Repo.insert! %Role{
  name: "employee"
}

Repo.insert! %Role{
  name: "manager"
}
