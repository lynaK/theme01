alias Todolist.Repo

  alias Todolist.Clock.Clocks

  Repo.insert! %Clocks{
    time: ~N[2020-10-23 14:00:00],
    status: true,
    users: 1
  }
