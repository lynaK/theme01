alias Todolist.Repo

alias Todolist.WorkingTimes.Workingtimes

  Repo.insert! %Workingtimes{
    start: ~N[2020-10-23 14:00:00],
    end: ~N[2020-10-23 19:00:00],
    users: 1
  }
