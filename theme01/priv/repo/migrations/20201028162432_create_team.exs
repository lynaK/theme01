defmodule Todolist.Repo.Migrations.CreateTeam do
  use Ecto.Migration

  def change do
    create table(:team) do
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:team, [:user_id])
  end
end
