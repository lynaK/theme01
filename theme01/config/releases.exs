# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
use Mix.Config

import Configdb_host = System.get_env("PGHOST") ||
  raise """
  environment variable DATABASE_HOST is missing.
  """
db_database ="timemanager_api_dev"
db_username ="postgres"
db_password ="admin"
db_host= "db"
db_url = "ecto://#{db_username}:#{db_password}@#{db_host}/#{db_database}"config :my_app, MyApp.Repo,
  url:  db_url,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")secret_key_base = System.get_env("iFBAu+7is6ogE6ZUcw1SfJzp4F17wzpwpapkeVeS4wQDpWat7UonQXm1OLsyyuuw") ||
  raise """
  environment variable SECRET_KEY_BASE is missing.
  You can generate one by calling: mix phx.gen.secret
  """
config :my_app, MyAppWeb.Endpoint,
  http: [:inet6, port: 4000],
  secret_key_base: secret_key_base

database_url =
  System.get_env("DATABASE_URL") ||
    raise """
    environment variable DATABASE_URL is missing.
    For example: ecto://USER:PASS@HOST/DATABASE
    """

config :timeManager_API, Todolist.Repo,
  # ssl: true,
  url: database_url,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :timeManager_API, TodolistWeb.Endpoint,
  http: [
    port: String.to_integer(System.get_env("PORT") || "4000"),
    transport_options: [socket_opts: [:inet6]]
  ],
  secret_key_base: secret_key_base

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
#     config :timeManager_API, TodolistWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.
