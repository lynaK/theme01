use Mix.Config# Used to generate urls
config :timeManager_API, TodolistWeb.Endpoint,
  url: [host: "localhost", port: 4000],
  cache_static_manifest: "priv/static/cache_manifest.json"# Do not print debug messages in production
config :logger, level: :info# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
config :timeManager_API, TodolistWeb.Endpoint,server: true
