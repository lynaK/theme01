import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import App from './App.vue'
import router from './router'
import 'vue-event-calendar/dist/style.css' //^1.1.10, CSS has been extracted as one file, so you can easily update it.
import vueEventCalendar from 'vue-event-calendar'
import Vuesax from 'vuesax'
// locale can be 'zh' , 'en' , 'es', 'pt-br', 'ja', 'ko', 'fr', 'it', 'ru', 'de', 'vi', 'ua', 'no, 'no-nn'
Vue.use(vueEventCalendar, {locale: 'en'}) 

Vue.prototype.$login = 1;
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.config.productionTip = false
Vue.use(axios)
Vue.use(Vuesax)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

