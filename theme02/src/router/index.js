import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.component('TheLogin', require('../components/LoginAndRegister.vue').default);
Vue.component('user-component', require('../components/UserComponent.vue').default);
Vue.component('form-component', require('../components/FormComponent.vue').default);
Vue.component('loginregister-component', require('../components/LoginAndRegister.vue').default);

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    alias: '/home',
    name: 'HomePage',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../components/HomePage.vue')
  },
  {
    path: '/user',
    name: 'User',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../components/UserComponent.vue')
  },
  {
    path: '/workingtimes',
    name: 'WorkingTimes',
    component: () => import('../components/WorkingTimes.vue')

  },
  {
    path: '/workingtime',
    name: 'WorkingTime',
    component: () => import('../components/WorkingTime.vue')

  },
  {
    path: '/chart',
    name: 'Chart',
    component: () => import('../components/ChartManager.vue')

  },
  {
    path: '/clock',
    name: 'Clock',
    component: () => import('../components/ClockManager.vue')

  }
  
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
