#if you launch the repo from windows please convert the entrypoint.sh file in theme01 repository (server)  in unix format, you can do it by notepad++

#use the command dokcer-compose up --build to launch the project from the root of the project, then go to localhost:8080 to check the website/ you can also see the requests from the server by tapping localhost:4000/api/"the_path_that_you_want_to_test"

#You can use the seeders in the theme01/repo/seeds repository(server)  to add fields in the tables of the database (the command will be launched automatically in the server's Dockerfile)

#to change the configuration of the database you can fill the fields in theme01/config/dev.exs

#Before launching docker-compose up --build, please drop down the database by 
#docker-compose down
#docker volume ls (it will display the name of the composant)
#docker volume rm *name of the composant*